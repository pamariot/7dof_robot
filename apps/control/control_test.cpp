#include "control.h"
#include "trajectory_generation.h"
#include <iostream>
using namespace std;

int main(){
  // Show using all three libraries, simulate the motion of a planar robot
  // For a initial position 
  //      q   = M_PI_2, M_PI_4
  // and  
  //      l1  = 0.4, l2 = 0.5
  // simulate a motion achieving 
  //      Xf  = 0.0, 0.6

  //double l1 = 0.4;
  //double l2 = 0.5;
  //Eigen::Vector2d q(M_PI_2, M_PI_4);
  //Eigen::Vector2d xf(0.0, 0.6);
//
  //RobotModel myModel = RobotModel(l1,l2);
  //Controller myController = Controller(myModel);
//
  //Eigen::Vector2d xi;
  //Eigen::Matrix2d J;
  //myModel.FwdKin(xi,J,q);
  //float Dt = 2;
  //float dt = 0.02;
//
  //auto trajectory = Point2Point(xi, xf, Dt);
//
  //Eigen::Vector2d x;
  //cout << "xd,yd,x,y,q1,q2,t\n";
//
  //for(float t=0; t<Dt+dt; t+=dt)
  //{
  //  auto xd = trajectory.X(t);
  //  auto Dxd_ff = trajectory.dX(t);
  //  Eigen::Vector2d dq = myController.Dqd(q,xd,Dxd_ff);
  //  myModel.FwdKin(x,J,q);
  //  cout << xd(0) << "," << xd(1) << "," << x(0) << "," << x(1) << "," << q(0) << "," << q(1) << "," << t << "\n";
  //  q += dq*dt;
  //}  

}