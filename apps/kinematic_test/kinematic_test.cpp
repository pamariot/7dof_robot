#include "kinematic_model.h"
#include <iostream>
using namespace std;

int main(){
  // Compute the forward kinematics and jacobian matrix for 
  //      q =  M_PI/3.0,  M_PI_4
  // For a small variation of q, compute the variation on X and check dx = J . dq  
  Eigen::Affine3d pose;
  Eigen::Matrix<double,6,7> J;
  Eigen::Vector7d q(M_PI_4/2, M_PI/7, M_PI/6, M_PI/5, M_PI_4, M_PI/3, M_PI_2);
  RobotModel MyModel = RobotModel();
  MyModel.FwdKin(pose, J, q);
  cout << "pos =\n" << pose.translation() << "\nrot=\n" << pose.rotation() << "\nJ =\n" << J << "\nfor q =\n" << q <<" \n";


  // analytic and numeric comparison
  Eigen::Vector7d q1(M_PI_4, M_PI_4, M_PI_4, M_PI_4, M_PI_4, M_PI_4, M_PI_4);
  Eigen::Vector7d dq(0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01);
  Eigen::Vector7d q2 = q1 + dq;

  Eigen::Vector6d pose1 = J*q1;
  Eigen::Vector6d pose2 = J*q2;
  Eigen::Vector6d dpose_n = pose2-pose1;
  Eigen::Vector6d dpose_a = J*dq;

  cout << "Comparing, dpose_a =\n" << dpose_a << "\nwhile dpose_n =\n" << dpose_n << "\n";  //seems good
}