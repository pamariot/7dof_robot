#include "trajectory_generation.h"
#include <iostream>
using namespace std;

int main(){
  // Compute the trajectory for given initial and final positions. 
  // Display some of the computed points
  // Check numerically the velocities given by the library 
  // Check initial and final velocities*
  //cout << "Trajectory test \n";
  Eigen::Vector3d Xi(0,0,0);
  Eigen::Matrix3d Ri; Ri << 1,0,0, 0,1,0, 0,0,1;
  Eigen::Affine3d Ti; Ti.translation() = Xi; Ti.linear() = Ri;
  Eigen::Vector3d Xf(1,2,3);
  Eigen::Matrix3d Rf; Rf << 1,0,0, 0,0,1, 0,-1,0;
  Eigen::Affine3d Tf; Tf.translation() = Xf; Tf.linear() = Rf;
  double Dt = 2;

  auto trajectory = Frame2Frame(Ti, Tf, Dt);

  cout << "t,x,y,z,dx,dy,dz,omegax,omegay,omegaz\n";

  for(double t=0; t<Dt; t+=0.2)
  {
    auto pose = trajectory.T(t); 
    auto vit = trajectory.dT(t);
    //Eigen::Vector2d vit_num(0,0);
    //vit_num = (trajectory.X(t+1e-6)-pos)/1e-6;
    cout << t << "," 
         << pose.translation()(0) << "," << pose.translation()(1) << "," << pose.translation()(2) << "," 
         << vit(0) << "," << vit(1) << "," << vit(2) << "," << vit(3) << "," << vit(4) << "," << vit(5) << "\n" ;
    //cout << "Position:\tx = " << pose.translation()(0) << "\ty = " << pose.translation()(1) << "\tz = " << pose.translation()(2) << "\n";
    //cout << "Vitesse:\tdx = " << vit(0) << "\tdy = " << vit(1) << "\tdz" << vit(2) << "\n";
    //cout << "Rotation:\n" << pose.rotation() << "\n";
    //cout << "Vitesse angulaire:\tomega_x = " << vit(3) << "\tomega_y = " << vit(4) << "\tomega_z = " << vit(5) << "\n\n";
  }
}