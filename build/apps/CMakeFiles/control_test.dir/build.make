# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.16

# Delete rule output on recipe failure.
.DELETE_ON_ERROR:


#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:


# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list


# Suppress display of executed commands.
$(VERBOSE).SILENT:


# A target that is always out of date.
cmake_force:

.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/bin/cmake

# The command to remove a file.
RM = /usr/bin/cmake -E remove -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /home/sleipnir/Bureau/prog/7dof_robot

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /home/sleipnir/Bureau/prog/7dof_robot/build

# Include any dependencies generated for this target.
include apps/CMakeFiles/control_test.dir/depend.make

# Include the progress variables for this target.
include apps/CMakeFiles/control_test.dir/progress.make

# Include the compile flags for this target's objects.
include apps/CMakeFiles/control_test.dir/flags.make

apps/CMakeFiles/control_test.dir/control/control_test.cpp.o: apps/CMakeFiles/control_test.dir/flags.make
apps/CMakeFiles/control_test.dir/control/control_test.cpp.o: ../apps/control/control_test.cpp
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir=/home/sleipnir/Bureau/prog/7dof_robot/build/CMakeFiles --progress-num=$(CMAKE_PROGRESS_1) "Building CXX object apps/CMakeFiles/control_test.dir/control/control_test.cpp.o"
	cd /home/sleipnir/Bureau/prog/7dof_robot/build/apps && /usr/bin/g++  $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -o CMakeFiles/control_test.dir/control/control_test.cpp.o -c /home/sleipnir/Bureau/prog/7dof_robot/apps/control/control_test.cpp

apps/CMakeFiles/control_test.dir/control/control_test.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/control_test.dir/control/control_test.cpp.i"
	cd /home/sleipnir/Bureau/prog/7dof_robot/build/apps && /usr/bin/g++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -E /home/sleipnir/Bureau/prog/7dof_robot/apps/control/control_test.cpp > CMakeFiles/control_test.dir/control/control_test.cpp.i

apps/CMakeFiles/control_test.dir/control/control_test.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/control_test.dir/control/control_test.cpp.s"
	cd /home/sleipnir/Bureau/prog/7dof_robot/build/apps && /usr/bin/g++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -S /home/sleipnir/Bureau/prog/7dof_robot/apps/control/control_test.cpp -o CMakeFiles/control_test.dir/control/control_test.cpp.s

# Object files for target control_test
control_test_OBJECTS = \
"CMakeFiles/control_test.dir/control/control_test.cpp.o"

# External object files for target control_test
control_test_EXTERNAL_OBJECTS =

apps/control_test: apps/CMakeFiles/control_test.dir/control/control_test.cpp.o
apps/control_test: apps/CMakeFiles/control_test.dir/build.make
apps/control_test: src/libcontrol.a
apps/control_test: src/libtrajectory_generation.a
apps/control_test: src/libkinematic_model.a
apps/control_test: apps/CMakeFiles/control_test.dir/link.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --bold --progress-dir=/home/sleipnir/Bureau/prog/7dof_robot/build/CMakeFiles --progress-num=$(CMAKE_PROGRESS_2) "Linking CXX executable control_test"
	cd /home/sleipnir/Bureau/prog/7dof_robot/build/apps && $(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/control_test.dir/link.txt --verbose=$(VERBOSE)

# Rule to build all files generated by this target.
apps/CMakeFiles/control_test.dir/build: apps/control_test

.PHONY : apps/CMakeFiles/control_test.dir/build

apps/CMakeFiles/control_test.dir/clean:
	cd /home/sleipnir/Bureau/prog/7dof_robot/build/apps && $(CMAKE_COMMAND) -P CMakeFiles/control_test.dir/cmake_clean.cmake
.PHONY : apps/CMakeFiles/control_test.dir/clean

apps/CMakeFiles/control_test.dir/depend:
	cd /home/sleipnir/Bureau/prog/7dof_robot/build && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /home/sleipnir/Bureau/prog/7dof_robot /home/sleipnir/Bureau/prog/7dof_robot/apps /home/sleipnir/Bureau/prog/7dof_robot/build /home/sleipnir/Bureau/prog/7dof_robot/build/apps /home/sleipnir/Bureau/prog/7dof_robot/build/apps/CMakeFiles/control_test.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : apps/CMakeFiles/control_test.dir/depend

