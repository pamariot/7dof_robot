#pragma once
#include "eigen3/Eigen/Dense"
#include "kinematic_model.h"

//! \brief Controller able to compute joint velocities for a given desired position
class Controller{
  public:
    //! \brief Controller of a 2-DoF robot described by a given model
    //! 
    //! \param model robot model
    Controller(RobotModel &model);

    //! \brief Joint velocities for given joint position, desired cartesian position and feedforward term
    //!
    //! \return Dqd     joint velocities      
    //! \param  q       joint position
    //! \param  Td      desired pose
    //! \param  DTd_ff  joint velocity feedforward term
    Eigen::Vector7d   Dqd ( 
              const Eigen::Vector7d & q,
              const Eigen::Affine3d & Td,
              const Eigen::Affine3d & DTd_ff
                          ); 
  private:
    RobotModel        model                                                ;    // Robot model
    const double      kp              { 1e2                               };    // Feedback gain
    Eigen::Matrix<double,6,7>   J     { Eigen::Matrix<double,6,7>::Zero() };    // Jacobian matrix
    Eigen::Affine3d   T               { Eigen::Affine3d::Identity()       };    // Frame pose
    Eigen::Matrix3d   dR_desired      { Eigen::Matrix3d::Identity()       };    // Desired angular velocity
    Eigen::Vector3d   dX_desired      { Eigen::Vector3d::Zero()           };    // Desired cartesian velocity
};