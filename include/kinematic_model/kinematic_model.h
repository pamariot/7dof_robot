#pragma once
#include "eigen3/Eigen/Dense"

namespace Eigen{
  typedef Matrix<double,8,1> Vector8d;
  typedef Matrix<double,7,1> Vector7d;
  typedef Matrix<double,6,1> Vector6d;
}

//! \brief Kinematic model of a 2-DoF robot (Forward and differential)
class RobotModel{
  public:
    //! \brief Kinematic model of panda robot
    //!
    RobotModel();

    //! \brief Compute transformation matrix between desired joint and previous one
    //!
    //! \param T Output transformation matrix
    //! \param joint desired joint for transformation
    //! \param q joint position
    void TransformMatrix(Eigen::Matrix<double,4,4> &T, const int &joint, const Eigen::Vector7d &qIn);
    //! \brief Compute pose and jacobian matrix for given joint position
    //!
    //! \param T Output end effector frame
    //! \param J Output jacobian matrix
    //! \param q joint position
    void FwdKin(Eigen::Affine3d &TOut, Eigen::Matrix<double,6,7> &JOut, const Eigen::Vector7d &qIn);
  private:
    Eigen::Vector8d a; // parametre a de DH
    Eigen::Vector8d alpha; // parametre alpha de DH
    Eigen::Vector8d r; // parametre r de DH
};