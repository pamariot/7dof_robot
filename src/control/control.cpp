#include "control.h"
#include <iostream>
Controller::Controller(RobotModel &rmIn):
  model  (rmIn)
{}

Eigen::Vector7d Controller::Dqd ( 
          const Eigen::Vector7d & q,
          const Eigen::Affine3d & Td,
          const Eigen::Affine3d & DTd_ff
                      )
{  
  // Compute joint velocities able to track the desired pose
  model.FwdKin(T,J,q);

    // position error correction
  dX_desired = (Td.translation()-T.translation())*kp + DTd_ff.translation();
    
    // angle-axis error correction
  Eigen::AngleAxisd Rd(Td.rotation());
  Eigen::AngleAxisd R(T.rotation());
  Eigen::AngleAxisd DRd_ff(DTd_ff.rotation());
  auto angle_desired = (Rd.angle()-R.angle())*kp + DRd_ff.angle();
  Eigen::Vector3d axis_desired = (Rd.axis()-R.axis())*kp + DRd_ff.axis();
  Eigen::Vector3d rotation_desired = angle_desired*axis_desired;
  
    // joint speed calculation
  Eigen::Matrix<double,6,1> transformation_parameters;
  transformation_parameters.block<3,1>(0,0) = dX_desired;
  transformation_parameters.block<3,1>(3,0) = rotation_desired;
  Eigen::Vector7d Dqd = J.completeOrthogonalDecomposition().pseudoInverse()*transformation_parameters;

  return Dqd; // returns joint velocities
}

