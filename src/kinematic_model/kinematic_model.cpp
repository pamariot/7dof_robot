#include "kinematic_model.h"

RobotModel::RobotModel()
{
  double a4 = 0.0825;
  double a5 = -0.0825;
  double a7 = 0.088;
  double d1 = 0.333;
  double d3 = 0.316;
  double d5 = 0.384;
  double df = 0.107;
  a << 0, 0, 0, a4, a5, 0, a7, 0;
  alpha << 0, -M_PI_2, M_PI_2, M_PI_2, -M_PI_2, M_PI_2, M_PI_2, 0;
  r << d1, 0, d3, 0, d5, 0, 0, df;
};

void RobotModel::TransformMatrix(Eigen::Matrix<double,4,4> &T, const int &joint, const Eigen::Vector7d &qIn)
{
  // Compute transformation matrix between desired joint and previous one
  Eigen::Vector7d q = qIn;
  T << cos(q(joint-1))                    ,-sin(q(joint-1))                    , 0                  , a(joint-1),
       cos(alpha(joint-1))*sin(q(joint-1)), cos(alpha(joint-1))*cos(q(joint-1)),-sin(alpha(joint-1)),-r(joint-1)*sin(alpha(joint-1)),
       sin(alpha(joint-1))*sin(q(joint-1)), sin(alpha(joint-1))*cos(q(joint-1)), cos(alpha(joint-1)), r(joint-1)*cos(alpha(joint-1)),
       0                                  , 0                                  , 0                  , 1;
}

void RobotModel::FwdKin(Eigen::Affine3d &TOut, Eigen::Matrix<double,6,7> &JOut, const Eigen::Vector7d &qIn)
{
  // Implementation of the forward and differential kinematics
  Eigen::Matrix<double,4,4> T01, T12, T23, T34, T45, T56, T67;
  TransformMatrix(T01, 1, qIn);
  TransformMatrix(T12, 2, qIn);
  TransformMatrix(T23, 3, qIn);
  TransformMatrix(T34, 4, qIn);
  TransformMatrix(T45, 5, qIn);
  TransformMatrix(T56, 6, qIn);
  TransformMatrix(T67, 7, qIn);
  Eigen::Matrix<double,4,4> T02 = T01 * T12;
  Eigen::Matrix<double,4,4> T03 = T02 * T23;
  Eigen::Matrix<double,4,4> T04 = T03 * T34;
  Eigen::Matrix<double,4,4> T05 = T04 * T45;
  Eigen::Matrix<double,4,4> T06 = T05 * T56;
  Eigen::Matrix<double,4,4> T07 = T06 * T67;

  TOut = T07;

  Eigen::Vector3d z1 = T01.block<3,1>(0,2);
  Eigen::Vector3d z2 = T02.block<3,1>(0,2);
  Eigen::Vector3d z3 = T03.block<3,1>(0,2);
  Eigen::Vector3d z4 = T04.block<3,1>(0,2);
  Eigen::Vector3d z5 = T05.block<3,1>(0,2);
  Eigen::Vector3d z6 = T06.block<3,1>(0,2);
  Eigen::Vector3d z7 = T07.block<3,1>(0,2);

  Eigen::Vector3d p1 = T01.block<3,1>(0,3);
  Eigen::Vector3d p2 = T02.block<3,1>(0,3);
  Eigen::Vector3d p3 = T03.block<3,1>(0,3);
  Eigen::Vector3d p4 = T04.block<3,1>(0,3);
  Eigen::Vector3d p5 = T05.block<3,1>(0,3);
  Eigen::Vector3d p6 = T06.block<3,1>(0,3);
  Eigen::Vector3d p7 = T07.block<3,1>(0,3);

  Eigen::Vector3d p17 = p7 - p1;
  Eigen::Vector3d p27 = p7 - p2;
  Eigen::Vector3d p37 = p7 - p3;
  Eigen::Vector3d p47 = p7 - p4;
  Eigen::Vector3d p57 = p7 - p5;
  Eigen::Vector3d p67 = p7 - p6;
  Eigen::Vector3d p77 = p7 - p7;

  Eigen::Vector3d zp1 = z1.cross(p17);
  Eigen::Vector3d zp2 = z2.cross(p27);
  Eigen::Vector3d zp3 = z3.cross(p37);
  Eigen::Vector3d zp4 = z4.cross(p47);
  Eigen::Vector3d zp5 = z5.cross(p57);
  Eigen::Vector3d zp6 = z6.cross(p67);
  Eigen::Vector3d zp7 = z7.cross(p77);

  JOut.block<3,7>(0,0) << zp1, zp2, zp3, zp4, zp5, zp6, zp7;
  JOut.block<3,7>(3,0) << z1, z2, z3, z4, z5, z6, z7;
}
